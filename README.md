# Analyzing TCIA Datasets

_This is mostly an educational exercise for myself, and a portfolio project, designed to re-familiarize myself with the DICOM imaging standard and provide some exposure to tools such as Spark and Tensorflow 2_

The Cancer Imaging Archive https://www.cancerimagingarchive.net/ (TCIA); provides free, anonymized datasets for public use. I've built a minimally viable ML data pipeline for medical images using PySpark, Spark SQL, Parquet and Avro files for storage as well as a convolutional neural network with residual blocks. 

## Table of contents

- [Overview](#overview)
- [Contents](#contents)
- [Challenges](#challenges)
- [Credits](#credits)
- [Citations](#citations)

## Overview

Images are gathered from public TCIA cancer repositories for the CHEST and BRAIN regions. I first set out with the simple goal of classifying each 512x512 grayscale image using the 'BodyPartExamined' metadata tag as the output label. There are only two classes in my final filtered data set to keep things simple for a proof of concept. 

DICOM files came in a semi-structured directory tree where folder names do not always, reliably, uniquely identify or index the data well. TCIA provided a csv with tabular annotations made by participants during an RSNA 2018 crowd-sourcing event, so I used these annotations to build a subset of the data suited for my ML model. 

Some things to note about the DICOM image format as it is used here, 
  
  - One file stores exactly one image, along with metadata located at particular byte offsets in the file
  - The 'SOPInstanceUID' metadata tag universally and uniquely identifies the image 
  - Each image is a 512x512 grayscale array with 16-bit encoding, 2 bytes per pixel 
  - Arrays may be encoded in signed or unsigned 16-bit integers

Screenshot of a single axial image slice with annotations around a tumor, 
  
<br />
<img src="https://gitlab.com/sthomanns/dicom-resnet/-/raw/master/archive/screenshot.png"  width="256" height="256">

## Contents

### a brief outline of the code structure, 

#### app/datastore.py
Loads the TCIA data using Spark SQL

#### app/instances.py
Recursively searches and identifies DICOM data on the file system

#### app/model.py
A residual convolutional neural net to interpret the images (work in progress)

#### app/viewers.py
Matplotlib calls that display the images along with their annotations

#### app/windowing.py
Logic for applying image window center and width and data normalization

#### data/instances.avro
Row-based storage of each image unique identifier and associated raw pixel array

#### data/metadata.parquet
Columnar-based storage of metadata associated with each image (such as annotations)

(not all data will be committed, as those files would take up too much space, but can be reconstructed in code) 

## Challenges

I'll use this section to go over some challenges I faced and trade-offs made in a few different areas. 

### Image Rescaling

All the images in my subset of data used a simple linear transormation using the Slope and Intercept DICOM metadata tags. I index all the images using a binary Avro file on disk, holding *only* the metadata needed to reconstruct each image into any one of it's possible presentation states (after rescaling, window leveling). All the images follow a simple linear scaling rule that should be applied when they are loaded from the Avro file, SV stands for each 'stored value' in a pixel array, 

        lambda SV, slope, intercept: (SV * slope) + intercept

### Image Windowing

Windowing augments the range of pixel values the image can take, changing brightness and contrast between anatomy. DICOM header tags specify recommended window center and window width settings. The window center is the mid-pixel in the range and width is the absolute size of the range all the pixel values need to fall within. 

Distribution of the pixel values at each image processing step are shown in the following histograms, ending with normalized values. 

<br />
<img src="https://gitlab.com/sthomanns/dicom-resnet/-/raw/master/archive/hist.png">

### Indexing & Data Cleansing

I am using a very small subset of the images available that match some specific criteria for the binary classifier: all images are 512x512, 16-bit (I intentionally filtered out some 12-bit grayscale images), and have a non-null 'BodyPartExamined' value in the metadata.  

Spark SQL loads a set of anonymized patient codes from the TCIA dataset saved in Parquet files, and partitions the data into subsets that can be processed in parallel on multiple cores. Each partition is filtered based on my criteria, and the results are written to the Avro file along with necessary metadata for image reconstruction. 

### TBD

Confusion Matrix TBD

<br />
<img src="https://gitlab.com/sthomanns/dicom-resnet/-/raw/master/archive/conf.png" width=256 height=256>

## Credits

Thank you to the free Cancer Imaging Archive for the NSCLC Radiogenomics and other DICOM datasets,

Raw Data,<br/>
https://wiki.cancerimagingarchive.net/display/Public/NSCLC+Radiogenomics#a99a795ff4454409862a398ffc076b98
https://wiki.cancerimagingarchive.net/display/Public/CPTAC-GBM

Annotations,<br/>
https://wiki.cancerimagingarchive.net/display/DOI/Crowds+Cure+Cancer%3A+Data+collected+at+the+RSNA+2018+annual+meeting

ResNet documentation,<br/>
https://adventuresinmachinelearning.com/introduction-resnet-tensorflow-2/

## Citations

Trinity Urban, Erik Ziegler, Steve Pieper, Justin Kirby, Daniel Rukas, Britney Beardmore, Bhanusupriya Somarouthu, Evren Ozkan, Gustavo Lelis, Brenda Fevrier-Sullivan, Samarth Nandekar, Andrew Beers, Carl Jaffe, John Freymann, David Clunie, Gordon J. Harris, Jayashree Kalpathy-Cramer. Crowds Cure Cancer: Data collected at the RSNA 2018 annual meeting. The Cancer Imaging Archive. doi: 10.7937/TCIA.2019.yk0gm1eb

Clark K, Vendt B, Smith K, Freymann J, Kirby J, Koppel P, Moore S, Phillips S, Maffitt D, Pringle M, Tarbox L, Prior F. The Cancer Imaging Archive (TCIA): Maintaining and Operating a Public Information Repository, Journal of Digital Imaging, Volume 26, Number 6, December, 2013, pp 1045-1057. (paper)

Bakr, Shaimaa; Gevaert, Olivier; Echegaray, Sebastian; Ayers, Kelsey; Zhou, Mu; Shafiq, Majid; Zheng, Hong; Zhang, Weiruo; Leung, Ann; Kadoch, Michael; Shrager, Joseph; Quon, Andrew; Rubin, Daniel; Plevritis, Sylvia; Napel, Sandy.(2017). Data for NSCLC Radiogenomics Collection. The Cancer Imaging Archive. http://doi.org/10.7937/K9/TCIA.2017.7hs46erv

Gevaert, O., Xu, J., Hoang, C. D., Leung, A. N., Xu, Y., Quon, A., … Plevritis, S. K. (2012, August). Non–Small Cell Lung Cancer: Identifying Prognostic Imaging Biomarkers by Leveraging Public Gene Expression Microarray Data—Methods and Preliminary Results. Radiology. Radiological Society of North America (RSNA). http://doi.org/10.1148/radiol.12111607

Clark K, Vendt B, Smith K, Freymann J, Kirby J, Koppel P, Moore S, Phillips S, Maffitt D, Pringle M, Tarbox L, Prior F. The Cancer Imaging Archive (TCIA): Maintaining and Operating a Public Information Repository, Journal of Digital Imaging, Volume 26, Number 6, December, 2013, pp 1045-1057. (paper)