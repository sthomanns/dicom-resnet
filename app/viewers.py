import numpy as np
import pydicom as dicom
import pylab 
import seaborn as sns 
import matplotlib.pyplot as plt
from bokeh.plotting import figure, show, output_file
from bokeh.models import HoverTool, ColumnDataSource
from collections import OrderedDict
from scipy import ndimage
from sklearn import cluster
from app.windowing import get_windowing, get_norm_HU, get_norm_window, window_image, IMG_RESCALE


def reconstruct(record): 
    """
    Reconstructs a SOP instance from the AVRO location on disk, taking
    into account slope and intercept, and normalizing the image array for
    input to the model. 

    Args:
        record (avro Record): a single image record from the AVRO file
    """
    sop_instance_bits_stored = record['sop_instance_bits_stored']
    sop_instance_pixel_rows = record['sop_instance_pixel_rows']
    sop_instance_pixel_columns = record['sop_instance_pixel_columns']
    sop_instance_pixel_representation = record['sop_instance_pixel_representation']
    sop_instance_reshaped = None
    _class = None

    if sop_instance_bits_stored == 16 \
        and sop_instance_pixel_rows == 512 \
        and sop_instance_pixel_columns == 512:  # and pixel_representation == 1:  #TODO

        sop_instance_rescale_slope = record['sop_instance_rescale_slope']
        sop_instance_rescale_intercept = record['sop_instance_rescale_intercept']
        sop_instance_body_part = record['sop_instance_body_part']
        sop_instance_window_center = record['sop_instance_window_center']
        sop_instance_window_width = record['sop_instance_window_width']
        sop_instance_pixel_array = record['sop_instance_pixel_array']
        sop_instance_pixel_samples = record['sop_instance_pixel_samples']
        sop_instance_reshaped = None

        raw = np.frombuffer(sop_instance_pixel_array, dtype = np.int16).copy()      #TODO
        rescaled = IMG_RESCALE(raw, sop_instance_rescale_slope, sop_instance_rescale_intercept) 
        windowed = window_image(rescaled, sop_instance_window_center, sop_instance_window_width)
        rows = sop_instance_pixel_rows
        cols = sop_instance_pixel_columns

        fn_norm = get_norm_window(sop_instance_window_center, sop_instance_window_width)
        sop_instance_norm = fn_norm(windowed, [])
        sop_instance_reshaped = sop_instance_norm.reshape((rows, cols))
        view_histograms(raw.reshape((rows, cols)), sop_instance_reshaped)
        
        # kmeans / dbscan
        #kmeans_cluster = cluster.KMeans(n_clusters=7)#cluster.KMeans(n_clusters=12)
        #kmeans_cluster.fit(sop_instance_reshaped)
        #cluster_centers = kmeans_cluster.cluster_centers_
        #cluster_labels = kmeans_cluster.labels_
        #plt.figure(figsize = (15,8))
        #plt.imshow(cluster_centers[cluster_labels])
        #plt.show()

        # One-hot Encode
        _class = None
        if sop_instance_body_part == 'SELLA':
            _class = [ 0.0, 1.0 ]
        elif sop_instance_body_part == 'CHEST':
            _class = [ 1.0, 0.0 ]

        assert _class is not None

    return (sop_instance_reshaped, _class )


def view_histograms(raw, normalized):
    """
    Displays the distribution of pixels in the images. 

    Args:
        raw (numpy Array):
        normalized (numpy Array):
    """
    kde_kws = { "color": "red", 'alpha': 0.0 }
    fig, axs = plt.subplots(2, 2) 
    sns.distplot(raw, ax=axs[0,1], hist_kws={ "linewidth": 1, "color": "orange" }, kde_kws=kde_kws)
    sns.distplot(normalized, ax=axs[1,1], hist_kws={ "linewidth": 1, "color": "darkred"}, kde_kws=kde_kws)
    axs[1,1].set_ylabel('Frequency')
    axs[1,1].set_xlabel('Stored Value (SV)')
    axs[0,0].imshow(raw, cmap=pylab.cm.bone)
    axs[1,0].imshow(normalized, cmap=pylab.cm.bone)
    plt.subplots_adjust(left=0.1,right=0.9,top=0.9,bottom=0.1)
    plt.show()

def view_instance(image, row):
    """
    Displays the annotated DICOM image for a specific lesion and
    patient

    Args:
        image (numpy Array): a 512x512 grayscale image array
        row (pyspark Row): the row from the case data containing
            the lesion coordinates
    """
    size = 8
    shortAxis_point1_x = row['ShortAxis_point1_x']
    shortAxis_point1_y = row['ShortAxis_point1_y']
    shortAxis_point2_x = row['ShortAxis_point2_x']
    shortAxis_point2_y = row['ShortAxis_point2_y']
    longAxis_point1_x = row['LongAxis_point1_x']
    longAxis_point1_y = row['LongAxis_point1_y']
    longAxis_point2_x = row['LongAxis_point2_x']
    longAxis_point2_y = row['LongAxis_point2_y']

    pylab.imshow(image, cmap=pylab.cm.bone) 
    pylab.scatter(shortAxis_point1_x, shortAxis_point1_y, s=size, c='red', marker='o')
    pylab.scatter(shortAxis_point2_x, shortAxis_point2_y, s=size, c='red', marker='o')
    pylab.scatter(longAxis_point1_x, longAxis_point1_y, s=size, c='red', marker='o')
    pylab.scatter(longAxis_point2_x, longAxis_point2_y, s=size, c='red', marker='o')
    pylab.show() 

def view_instance_at_path(path, row):
    """
    Displays the annotated DICOM image for a specific lesion and
    patient, using the path instead of the raw image

    Args:
        path (string): file path of the SOP instance
        row (pyspark Row): the row from the case data containing
            the lesion coordinates
    """
    sop_instance = dicom.read_file(path)
    sop_instance_pixel_array = sop_instance.pixel_array
    rescale_slope = sop_instance.RescaleSlope
    rescale_intercept = sop_instance.RescaleIntercept

    # Apply the windowing which is specified in the DICOM header 
    assert rescale_slope == 1 and rescale_intercept == -1024
    window_center, window_width, intercept, slope, padding = get_windowing(sop_instance)

    # Rescale and window level 
    image_rescaled = IMG_RESCALE(sop_instance_pixel_array, slope, intercept)
    image_windowed = window_image(image_rescaled, window_center, window_width)

    # These are normalized arrays for the purposes of the ML model 
    normed_HU = get_norm_HU(padding)(sop_instance_pixel_array, [])
    normed_window = get_norm_window(window_center, window_width)(image_windowed, [])

    # Histograms 
    sns.distplot(normed_HU, label='HU (Normalized)')
    sns.distplot(normed_window, label='Windowed (Normalized)')
    plt.show()

    # Draw the points around the lesion and show them    
    view_instance(image_windowed, row)       
