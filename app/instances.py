import os
import pydicom as dicom
import pyspark as spark
import pandas as pd
import numpy as np
import glob 
from pydicom import pixel_data_handlers
from fastavro import writer, reader, parse_schema
from fastparquet import write, ParquetFile
from app.datastore import get_case_context, get_case_data, get_case_instance
from app.viewers import view_instance, view_instance_at_path, reconstruct
from app.windowing import get_norm_HU, get_norm_window, get_windowing, window_image, IMG_RESCALE

# ENV: conda env export | grep -v "^prefix: " > environment.yml
# https://www.youtube.com/watch?v=6vCVuIx64Rk
# NON-stationary time series appear stationary over certain time intervals
# https://clinicalapi-cptac.esacinc.com/api/tcia/
# https://wiki.cancerimagingarchive.net/display/Public/NSCLC+Radiogenomics#a99a795ff4454409862a398ffc076b98
# https://wiki.cancerimagingarchive.net/display/DOI/Crowds+Cure+Cancer%3A+Data+collected+at+the+RSNA+2018+annual+meeting
# https://groups.google.com/forum/#!topic/pydicom/q-CtWdFn-Mk
# https://eng.uber.com/hdfs-file-format-apache-spark/
# https://papers.nips.cc/paper/5656-hidden-technical-debt-in-machine-learning-systems.pdf

DIRNAME = os.path.dirname(os.path.abspath(__file__))
DIRNAME_EXT = "/Volumes/Elements"

IMG_SCHEMA = {
    'doc': 'DICOM SOP Instances',
    'name': 'DICOM',
    'namespace': 'dicom-resnet',
    'type': 'record',
    'fields': [
        {'name': 'sop_instance_rescale_slope', 'type': 'int', 'order': 'ignore'},
        {'name': 'sop_instance_rescale_intercept', 'type': 'int', 'order': 'ignore'},
        {'name': 'sop_instance_window_center', 'type': 'int', 'order': 'ignore'},
        {'name': 'sop_instance_window_width', 'type': 'int', 'order': 'ignore'},
        {'name': 'sop_instance_bits_stored', 'type': 'int', 'order': 'ignore'},
        {'name': 'sop_instance_body_part', 'type': 'string', 'default': 'NONE', 'order': 'ignore'},
        {'name': 'sop_instance_pixel_array', 'type': 'bytes', 'order': 'ignore'},
        {'name': 'sop_instance_pixel_rows', 'type': 'int', 'order': 'ignore'},
        {'name': 'sop_instance_pixel_columns', 'type': 'int', 'order': 'ignore'},
        {'name': 'sop_instance_pixel_representation', 'type': 'int', 'order': 'ignore'},
        {'name': 'sop_instance_pixel_samples', 'type': 'int', 'order': 'ignore'},
        {'name': 'sort_attr', 'type': 'int', 'order': 'ascending'}
    ],
}
IMG_SCHEMA = parse_schema(IMG_SCHEMA)


def reconstruct_images():
    """
    Reconstructs SOP instances from the AVRO file 
    TODO: I use a simple algorithm that assumes images with the same sum() across all pixels are equivalent (even 
            though this is an imperfect solution, a better solution might use a hash collision approach). 
            - Compute np.sum() for each image 
            - Sort the images in ascending order by their sum
            - Iterate the dataset, reconstrucing each image and image label 
            - During reconstruction, exclude any consecutive images with identical sums from the dataset
    """
    _x = []
    _y = []
    _fn_sort = lambda record: np.sum(np.frombuffer(record['sop_instance_pixel_array'], dtype=np.int16))
    _last_sum = 0 

    # Sort the AVRO file contents and iterate once, reconstructing each image and checking for duplicates 
    with open(f"./data/instances.avro", 'rb') as fo:
        for record in reader(fo): #sorted(reader(fo), key=_fn_sort):

            # Reconstruct the image 
            _x_image, _y_label = reconstruct(record)
            _curr_sum = record['sort_attr'] #_fn_sort(record)

            # Prevent duplicates by checking for images with consecutive equivalent sums in a pre-sorted array (sorted by sum)
            if _x_image is not None and ( _last_sum != _curr_sum ):
                _x.append(_x_image)
                _y.append(_y_label)
                _last_sum = _curr_sum

    _x = np.array(_x)
    _x = _x[..., np.newaxis].astype(np.float32)
    _y = np.array(_y).astype(np.float32)

    # Shuffle the _x and _y dimension using the same random seed, to shuffle them the exact same way 
    rng_state = np.random.get_state()
    np.random.shuffle(_x)
    np.random.set_state(rng_state)
    np.random.shuffle(_y)

    return (_x, _y)


def view_image(patient_id='AMC-042', sop_instance_uid='1.3.6.1.4.1.14519.5.2.1.4334.1501.220617359288106679210852140138'):
    """
    Displays the annotated DICOM image for a specific lesion and patient, by finding
    the file path and querying the associated annotations. 

    Args: 
        patient_id (string): the DICOM patient ID
        sop_instance_uid (string): the DICOM SOP instance UID
    """
    case_context, columns = get_case_context()
    query = get_case_instance(case_context, patient_id, sop_instance_uid)
    query_row = query.take(1)[0].asDict()

    path_indices = ParquetFile(f"./data/metadata.parq")
    path_indices = path_indices.to_pandas()
    path = path_indices[path_indices['ReferencedSOPInstanceUID'] == query_row['ReferencedSOPInstanceUID']] 

    view_instance_at_path(path['Path'].item(), query_row)

def index_images(case_collection): 
    """
    Indexes the dataset using the parquet file at /data/CrowdsCureCancer2018-Results.parq, so
    that for every patient with annotations, the associated file path for that SOP instance is 
    saved to a lookup table. 
    """
    case_context, columns = get_case_context()
    case_subjects = get_case_data(case_context, case_collection)
    result = case_subjects.rdd \
                        .mapPartitionsWithIndex(lambda x,y: (x,y), preservesPartitioning = True) \
                        .foreachPartition(lambda p: index_write(p, case_collection))
    return result;        


def index_write(partition, case_collection):
    """
    Indexes a single patient and all SOP instances within that patients' DICOM dataset
    on the file system. 

    Args:
        partition (itertools.chain): partition of the data returned from the 
            spark SQL case dataset
    """
    partition_id = partition[0]
    partition_data = partition[1]
    all_metadata = []
    #all_images = []

    images_file = f"./data/instances.avro"#_{partition_id}.avro"
    metadata_file = f"./data/metadata.parq"

    #with open(images_file, 'ab+') as out:
    for row in partition_data:
        row_patient = row['SubjectID']
        print('on case = ' + case_collection)
        print('on patient = ' + str(row_patient))

        for i in glob.iglob(f"{DIRNAME_EXT}/__dicomcache__/{case_collection}/{row_patient}/**/*.dcm", recursive=True):
            sop_instance = dicom.read_file(i, defer_size="512 KB")
            sop_instance_uid = sop_instance.SOPInstanceUID

            if hasattr(sop_instance, 'BodyPartExamined'):
                if sop_instance.BitsStored == 16 and sop_instance.Rows == 512 and sop_instance.Columns == 512 and sop_instance.Modality == 'MR':

                    window_center, window_width, intercept, slope, padding = get_windowing(sop_instance)
                    img_bytes = sop_instance[('7FE0','0010')].value
                    # TODO padding
                    # TODO 
                    # PixelRepresentation = 0 -> unsigned
                    # PixelRepresentation = 1 -> signed

                    img_sum = sum(img_bytes)
                    print('sum=')
                    print(img_sum)

                    all_metadata.append([ sop_instance_uid, i ])
                    img = { 
                        'sop_instance_rescale_slope': slope,
                        'sop_instance_rescale_intercept': intercept,
                        'sop_instance_window_center': window_center,
                        'sop_instance_window_width': window_width,
                        'sop_instance_bits_stored': sop_instance.BitsStored,
                        'sop_instance_body_part': sop_instance.BodyPartExamined,
                        'sop_instance_pixel_array': img_bytes,
                        'sop_instance_pixel_rows': sop_instance.Rows,
                        'sop_instance_pixel_columns': sop_instance.Columns,
                        'sop_instance_pixel_representation': sop_instance.PixelRepresentation,
                        'sop_instance_pixel_samples': sop_instance.SamplesPerPixel,
                        'sort_attr': img_sum
                    }
                    reconstruct(img)

                    '''
                    with open(images_file, 'ab+') as out:
                        writer(out, IMG_SCHEMA, [img])
                    '''
                    
    metadata_dataframe = pd.DataFrame(all_metadata, columns = ['ReferencedSOPInstanceUID', 'Path']) 
    #TODO can query avro directly by storing byte offsets

    if os.path.isfile(metadata_file):
        write(metadata_file, metadata_dataframe, compression='GZIP', append=True)
    else:
        write(metadata_file, metadata_dataframe, compression='GZIP', append=False)

    return all_metadata







'''
def index_images(case_collection): 
    """
    Indexes the dataset using the parquet file at /data/CrowdsCureCancer2018-Results.parq, so
    that for every patient with annotations, the associated file path for that SOP instance is 
    saved to a lookup table. 
    """
    case_context, columns = get_case_context()
    case_subjects = get_case_data(case_context, case_collection)
    result = case_subjects.rdd \
                        .mapPartitionsWithIndex(lambda x, y: process_partition(x, y, case_collection), preservesPartitioning = True) \
                        .foreachPartition(lambda p: index_write(p, case_collection)) \
                        .collect()
    return result;    

def process_partition(id, data, case_collection): 
    for row in data:
        row_patient = row['SubjectID']

        for i in glob.iglob(f"{DIRNAME_EXT}/__dicomcache__/{case_collection}/{row_patient}/**/*.dcm", recursive=True):
            sop_instance = dicom.read_file(i, defer_size="512 KB")
            sop_instance_uid = sop_instance.SOPInstanceUID

            if hasattr(sop_instance, 'BodyPartExamined'):
                if sop_instance.BitsStored == 16 and sop_instance.Rows == 512 and sop_instance.Columns == 512:

                    window_center, window_width, intercept, slope, padding = get_windowing(sop_instance)
                    img_bytes = sop_instance[('7FE0','0010')].value

                    #all_metadata.append([ sop_instance_uid, i ])
                    img = { 
                        'sop_instance_rescale_slope': slope,
                        'sop_instance_rescale_intercept': intercept,
                        'sop_instance_window_center': window_center,
                        'sop_instance_window_width': window_width,
                        'sop_instance_bits_stored': sop_instance.BitsStored,
                        'sop_instance_body_part': sop_instance.BodyPartExamined,
                        'sop_instance_pixel_array': img_bytes,
                        'sop_instance_pixel_rows': sop_instance.Rows,
                        'sop_instance_pixel_columns': sop_instance.Columns,
                        'sop_instance_pixel_representation': sop_instance.PixelRepresentation,
                        'sop_instance_pixel_samples': sop_instance.SamplesPerPixel
                    }
                    yield img 

def index_write(partition, case_collection):
    images_file = f"./data/instances.avro"#_{partition_id}.avro"
    metadata_file = f"./data/metadata.parq"
    with open(images_file, 'ab+') as out:   
        for img in partition:
            writer(out, IMG_SCHEMA, [img])
'''