import pyspark as spark
from pyspark.sql import SQLContext, SparkSession

# A tumor has long and short dimensions specified by (x,y) co-
# ordinates which approximate the boundaries of the tumor. The
# annotations were made at a crowd sourcing event at RSNA 2018
# by a group of clinicians. This will be the basis for a ground 
# truth dataset. 
#
# Explanation of DICOM data hierarchy (series level omitted)
#
#   Top-level, SubjectID (also known as Patient ID, Accession Number) is unique to a patient
#   Mid-level, StudyInstanceUID (a patient may have multiple studies)
#   Low-level, ReferencedSOPSequence.ReferencedSOPInstanceUID uniquely identifies a slice or 
#               specific image where tumor (lesion) is identifiable
#
#   A single .dcm file includes the above metadata, and more, as well as the grayscale pixel 
#   array in the same file (last 512x512x2 bytes for a 512x512 image)
#
ANNOTATIONS = [ 'Collection',
                'SubjectID',
                'StudyInstanceUID',
                'ReferencedSOPInstanceUID',
                'ShortAxis_point1_x', 
                'ShortAxis_point1_y', 
                'ShortAxis_point2_x', 
                'ShortAxis_point2_y', 
                'LongAxis_point1_x', 
                'LongAxis_point1_y', 
                'LongAxis_point2_x', 
                'LongAxis_point2_y' ]


def get_case_context():
    """ 
    Creates a spark SQLContext from the RSNA 2018 crowd sourcing event
    data where Radiologists annotated these DICOM cases voluntarily in 
    a tournament style. Data provided by the Cancer Imaging Archive.

    Returns:
        sql_context, sql_columns (tuple): the sql context, a list of 
            columns in the table
    """
    spark = SparkSession.builder \
        .master("local[4]") \
        .appName("dicom-resnet") \
        .config("spark.sql.shuffle.partitions", "4") \
        .getOrCreate()
    sql_context = SQLContext(spark)
    sql_store = sql_context.read.parquet(f"./data/CrowdsCureCancer2018-Results.parq")
    sql_store.registerTempTable("annotations")
    sql_columns = ',annotations.'.join(ANNOTATIONS)
    return (sql_context, sql_columns)

def get_case_data(sql_context, case_collection): 
    """ 
    Gets annotations on all images for a specific collection of cases 
    from the archive 

    Returns:
        multi_case (pyspark.sql.DataFrame): the results of
            the query in a spark dataframe
    """
    multi_case = sql_context.sql((
        f"SELECT DISTINCT SubjectID \n"
        f"FROM annotations \n"
        f"WHERE Collection = '{case_collection}' \n"
    ))
    return multi_case

def get_case_instance(sql_context, patient_id, sop_instance_uid): 
    """ 
    Gets annotations on a specific image SOP instance

    Returns:
        single_case (pyspark.sql.DataFrame): the results of
            the query in a spark dataframe
    """
    # TODO may need to rebuild the .parq annotations file w/ more collections
    single_case = sql_context.sql((
        f"SELECT * \n"
        f"FROM annotations \n"
        f"WHERE SubjectID = '{patient_id}' \n"
        f"AND ReferencedSOPInstanceUID = '{sop_instance_uid}' \n"
    ))
    return single_case