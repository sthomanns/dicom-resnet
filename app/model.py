import os
import numpy as np
import datetime as dt
import tensorflow as tf
from keras.metrics import categorical_accuracy
from keras.utils import to_categorical
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.metrics import classification_report, confusion_matrix
from app.instances import reconstruct_images


EPOCHS = 36 

def run_model(): 
  """
  Residual Convolutional Neural Net

  The classification task is to differentiate images based on the DICOM 'BodyPartExamined' 
  tag. Categorical cross entropy as the loss function with one-hot encoded inputs and outputs, 
  for classifying a brain image from a chest image. 
  """
  _x, _y = reconstruct_images()

  # Split the data into train, validate, and test sets
  x_train = _x[:380]
  y_train = _y[:380]
  x_validate = _x[381:421]
  y_validate = _y[381:421]
  x_test = _x[422:472]
  y_test = _y[422:472]

  # Define the input shape and convolutional layers 
  inputs = keras.Input(shape=( 512, 512, 1 ))
  x = layers.Conv2D(32, 3, activation='relu')(inputs)
  x = layers.Conv2D(64, 5, activation='relu')(x)
  x = layers.MaxPooling2D(3)(x)
  num_res_net_blocks = 4

  # Builds a residual block 
  def res_net_block(input_data, filters, conv_size):
    x = layers.Conv2D(filters, conv_size, activation='relu', padding='same')(input_data)
    x = layers.BatchNormalization()(x)
    x = layers.Conv2D(filters, conv_size, activation=None, padding='same')(x)
    x = layers.BatchNormalization()(x)
    x = layers.Add()([x, input_data])
    x = layers.Activation('relu')(x)
    return x

  for i in range(num_res_net_blocks):
      x = res_net_block(x, 64, 5)
  x = layers.Conv2D(128, 5, activation='relu')(x)
  x = layers.GlobalAveragePooling2D()(x)
  x = layers.Dense(256, activation='relu')(x)

  # 2 dense layers for encoded output [ 0, 1 ][ 1, 0 ]
  outputs = layers.Dense(2, activation='softmax')(x)
  res_net_model = keras.Model(inputs, outputs)

  dirname = os.path.dirname(os.path.abspath(__file__))
  now = dt.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
  callbacks = [
    keras.callbacks.TensorBoard(log_dir=f"{dirname}/__log__/{now}", write_images=True)
  ]

  # Compile, fit, and evaluate the model on validation and test data 
  res_net_model.compile(optimizer=keras.optimizers.Adam(), loss='categorical_crossentropy', 
                metrics=[categorical_accuracy])
  res_net_model.fit(x_train, y_train, epochs=EPOCHS, validation_data=(x_validate, y_validate), 
                callbacks=callbacks)

  scores = res_net_model.evaluate(x_test, y_test, verbose=0)
  print('Baseline error: %.2f' % (1 - scores[1]))

  # argmax() returns the class output from the encoding of a probability dist in form [ 0.49, 0.51 ]
  y_predtr = res_net_model.predict(x_train)
  y_predtr_argmax = tf.argmax(y_predtr, axis=1)
  y_predtr = to_categorical(y_predtr_argmax)

  y_pred = res_net_model.predict(x_test)
  print("Y(Pred Probabilities) \n=======================")
  print(y_pred)

  y_pred_argmax = tf.argmax(y_pred, axis=1)
  y_pred = to_categorical(y_pred_argmax)
  y_test_argmax = tf.argmax(y_test, axis=1)
  y_test = to_categorical(y_test_argmax)

  print("Y(Test) \n=======================")
  print(y_test)

  print("Y(Pred) \n=======================")
  print(y_pred)

  print("Y(Test argmax()) \n=======================")
  print(y_test_argmax)

  print("Y(Pred argmax()) \n=======================")
  print(y_pred_argmax)

  print("Classification report \n=======================")
  print(classification_report(y_true=y_test, y_pred=y_pred))

  print("Confusion matrix \n=======================")
  print(confusion_matrix(y_true=y_test_argmax, y_pred=y_pred_argmax))

  #print('counts=')
  #(u, c) = np.unique(tf.argmax(_y, axis=1), return_counts=True)
  #freq_arr = np.asarray((u, c)).T
  #print(freq_arr)