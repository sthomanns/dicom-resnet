import os
from flask import Flask, request, send_from_directory

flaskApp = Flask(__name__)

@flaskApp.route('/<file>')
def static_root(file):
    return send_from_directory(os.path.join('.', 'client'), file)
