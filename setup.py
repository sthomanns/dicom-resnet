from setuptools import setup

setup(
   name='app',
   version='1.0',
   description='dicom-resnet',
   author='Sean Thoman',
   author_email='sthoman@protonmail.com',
   packages=['app']
)