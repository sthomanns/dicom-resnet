import os
import sys
from app import view_image, index_images, run_model, flaskApp

CASE_COLLECTIONS = [ 'CPTAC-GBM' ]  #[ 'NSCLC Radiogenomics', 'CPTAC-GBM' ] #[ 'CT COLONOGRAPHY' ]

if __name__ == "__main__": 

    os.environ['PYSPARK_PYTHON'] = sys.executable
    os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable
    
    #flaskApp.run()

    #view_image()
    #for i in CASE_COLLECTIONS: 
    #    index_images(i)
    run_model()